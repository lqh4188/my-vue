# myVue

## 介绍

Vue2+element 练习项目，包含 vuex、??/?. 可选链语法和双问号语法 sass 、vuex-along 配置

## 项目目录

```
.
├── public #公共资源文件夹 │
├── src #源代码⽂件夹
│ ├── api #后台接口配置 │
│ ├── assets #项目静态资源
│ ├── config #项目环境变更配置
│ ├── plugin #项目插件
│ ├── static #项目公用样式、图标 │ │
│ ├── utils #项目工具类文件夹，例如：自定义指令、检验、公用函数等 js 文件
│ ├── views #项目源码
│ │ └── index.vue # 项目演示主页页面
│ └── main.js #项⽬的核⼼配置⽂件
├── .eslintrc.js #eslint 配置
├── .prettierrc #代码格式化配置
├── package-lock.json #依赖锁定⽂件
├── package.json #项⽬和依赖描述
└── README.md #项⽬的说明⽂件
```

### 安装教程

1.  npm i --安装依赖
2.  npm run serve --启动项目

### 使用说明

1. vue (?.)可选链语法 (??)双问号语法配置
   需要 babel7 以上的版本支持，添加以下两个依赖
   @babel/plugin-proposal-optional-chaining // 可选链
   @babel/plugin-proposal-nullish-coalescing-operator // 双问号
   然后在 babel.config.js 中这加入 2 个插件

```
 plugins: [
        "@babel/plugin-proposal-optional-chaining",//解析 可选链语法
        "@babel/plugin-proposal-nullish-coalescing-operator"//解析 双问号语法
    ]
```

2. vuex 使用

### 参与贡献
