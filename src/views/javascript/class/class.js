// 构建函数，类的共用属性放在constructor里
class Person {
  constructor(name, age) {
    this.name = name;
  }
}
class Student extends Person {
  // 子类继承父类
  constructor(name, other) {
    super(name); // 调用父类的constructor
    this.other = other; // 定义子类的属性
  }
}

var p1 = new Person('刘德华', 40);
console.log('log内容', p1);
//1 通过class关键字创建类，首字母大写
//2 constructor函数，可以接受传递过来的参数，同时返回实例对象
//3 constructor只要new 生成实例时，就会自动调用这个函数，如果不写，类也会自动生成这个函数
//4 生成实例,new 不能省略
//5 创建类，类名后不加小括号，生成实例 类名后面要加小括号，构建函数不需要function

// 1 使用父构造函数
function Father(name, age) {
  // this指向父构造函数
  this.name = name;
  this.age = age;
}
Father.prototype.money = function() {
  console.log('function money');
};
function Son(name, age, score) {
  // 通过call使Father的this指向Son
  Father.call(this, name, age);
  this.score = score;
}
Son.prototype = new Father();
// 如果利用对象的形式修改了原型对象，要使用constructor 指回原来的构造函数
Son.prototype.constrctor = Son;
// 子类的方法
Son.prototype.exam = function() {
  console.log('子类的专属方法');
};

var son = new Son('刘德华', 18);
console.log('son:', son);
