
// import { setToken, setUserInfo, getUserInfo, clearStore } from '../utils/token.js';
// import api from '../api/index.js'

const test = {
    state: {
        blogTitle: '迩伶贰blog',
        views: 5,
        blogNumber: 100,
        total: 0,
        todos: [
            { id: 1, done: true, text: '我是码农' },
            { id: 2, done: false, text: '我是码农202号' },
            { id: 3, done: true, text: '我是码农202号' }
        ]
    },
    mutations: {
        addViews(state) {
            state.views++
        },
        blogAdd(state) {
            state.blogNumber++
        },
        clickTotal(state) {
            state.total++
        }
    },
    actions: {
        storeActaddViews({ commit }) {
            commit('addViews')
        },
        storeActclickTotal({ commit }) {
            commit('clickTotal')
        },
        storeActblogAdd({ commit }) {
            commit('blogAdd')
        }
    },
    modules: {
        getToDo(state) {
            return state.todos.filter(item => item.done === true)
            // filter 迭代过滤器 将每个item的值 item.done == true 挑出来， 返回的是一个数组
        }
    }
}

export default test;