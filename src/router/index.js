import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);
const routerPush = VueRouter.prototype.push;

VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error => error);
};

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'index',
      component: () => import('@/views/index.vue')
    },
    {
      path: '/test',
      name: 'test',
      component: () => import('@/views/test.vue')
    },
    {
      path: '/vueis',
      name: 'vueis',
      component: () => import('@/views/vue-is')
    },
    {
      path: '/iframe',
      name: 'iframe',
      component: () => import('@/views/iframe/index.vue')
    },
    {
      path: '/render',
      name: 'render',
      component: () => import('@/views/render/index.vue')
    },
    {
      path: '/inject',
      name: 'inject',
      component: () => import('../views/inject/index.vue')
    },
    {
      path: '/validate',
      name: 'validate',
      component: () => import('../views/validate/index.vue')
    },
    {
      path: '/props',
      name: 'props',
      component: () => import('../views/props/index.vue')
    },
    {
      path: '/hook',
      name: 'hook',
      component: () => import('@/views/hook/index.vue')
    },
    {
      path: '/vmodel',
      name: 'vmodel',
      component: () => import('@/views/vmodel/index.vue')
    },
    {
      path: '/money',
      name: 'money',
      component: () => import('@/views/money/index.vue')
    },
    {
      path: '/mathexpress',
      name: 'mathexpress',
      component: () => import('@/views/mathexpress/index.vue')
    },
    //使用冒号标记，当匹配到的时候，参数值会被设置到this.$route.params中
    {
      path: '/user/:name',
      name: 'user',
      component: () => import('@/views/params/index.vue')
    },
    {
      path: '/iframeCall',
      name: 'iframeCall',
      component: () => import('@/views/demo/iframe.vue')
    },
    {
      path: '/vuePrismEditor',
      name: 'vuePrismEditor',
      component: () => import('@/views/demo/vuePrismEditor.vue')
    },
    {
      path: '/aceEditor',
      name: 'aceEditor',
      component: () => import('@/views/demo/vue2-ace-editor.vue')
    }
  ]
});

router.beforeEach(async (to, from, next) => {
  // to: Route: 即将要进入的目标 路由对象
  // from: Route: 当前导航正要离开的路由
  //如果next为空则路由正常进行跳转，如果next不为空，则进行跳转时，会中断
  // 未登录状态；路由跳转至login
  // 已登录状态；当路由到login时，如果已经登录过，则跳转至home

  // if (to.name === 'login') {
  //     if (isLogin) {
  //         await routerTo();
  //     }
  // } else {
  //     if (!isLogin && to.name != 'forget') {
  //         router.push({ name: 'login' })
  //     }
  // }
  next();
});
export default router;
