import { get, post } from "./axios"
const axios = {
    //Auth认证
    authCurrentUser: (data) => post("/api/auth/currernt-user", data),
    authPermissions: (data) => post("/api/auth/permissions", data),   
}

export default axios