var CryptoJS = require("crypto-js");
var aesutil = module.exports = {};
/**
 * aes加密
 * @param data 待加密内容
 * @param key 必须为32位私钥
 * @returns {string}
 */
aesutil.encryption = function (data, key, iv) {
    const formatedKey = CryptoJS.enc.Utf8.parse(key); // 将 key 转为 128bit 格式
    const formatedIv = CryptoJS.enc.Utf8.parse(iv || key.split("").reverse().join("")); // 将 iv 转为 128bit 格式
    const encrypted = CryptoJS.AES.encrypt(data, formatedKey, { iv: formatedIv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }); // 加密
    return encrypted.ciphertext.toString();
}
/**
 * aes解密
 * @param data 待解密内容
 * @param key 必须为32位私钥
 * @returns {string}
 */
aesutil.decryption = function (data, key, iv) {
    const encryptedHexStr = CryptoJS.enc.Hex.parse(data); // 把密文由 128 bit 转为十六进制
    const encryptedBase64Str = CryptoJS.enc.Base64.stringify(encryptedHexStr); // 再转为 Base64 编码的字符串
    const formatedKey = CryptoJS.enc.Utf8.parse(key); // 将 key 转为 128bit 格式
    const formatedIv = CryptoJS.enc.Utf8.parse(iv || key.split("").reverse().join("")); // 将 iv 转为 128bit 格式
    const decryptedData = CryptoJS.AES.decrypt(encryptedBase64Str, formatedKey, { iv: formatedIv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }); // 解密
    return decryptedData.toString(CryptoJS.enc.Utf8); // 经过 AES 解密后，依然是一个对象，将其变成明文就需要按照 Utf8 格式转为字符串
}