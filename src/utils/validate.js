/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validNum(str) {
    const reg = /^[0-9]*$/
    return reg.test(str)
}
/**
 * @param {string} str
 * @param {number} len
 * @param {string} opt ==  >
 * @returns {Boolean}
 */
export function validNumRegLen(str, len, operator) {
    let cus = true;
    if (len) {
        if (!str || str == "") {
            cus = false;
        } else {
            switch (operator) {
                case '==':
                    cus = str.length == len;
                    break;
                case '!=':
                    cus = str.length != len;
                    break;
                case '>':
                    cus = str.length > len;
                    break;
            }
        }
    }
    const reg = /^[0-9xX\*]*$/
    return reg.test(str) && cus;
}


/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validString(str) {
    if (str.length == 0 || str.length > 50) {
        return false;
    }
    return true;
}
/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validLetterOrNum(str) {
    const reg = /^[a-zA-Z\d]+$/
    return reg.test(str)
}

export function validatorNoHanZi(rule, value, callback) {
    let res = /[^\w\.\/]/
    if (!res.test(value)) {
        callback()
    } else {
        callback(new Error("只能输入字母或数字，不能使用汉字及特殊符号"))
    }
}
export function validateEmail(rule, value, callback) {
    let res = /^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/
    if (!value || res.test(value)) { //为空时不校验
        callback()
    } else {
        callback(new Error("邮箱格式不正确"))
    }
}
/**
 * 验证18位身份证号
 */
export function validIdentity(rule, value, callback) {
    let reg = /^[0-9\*]{17}[0-9xX]$/;
    if (!value || (value.length == 18 && reg.test(value))) { //为空时不校验
        callback()
    } else {
        callback(new Error("请输入18位有效身份证号"))
    }
}
/**
 * 验证手机号
 */
export function validMobilePhone(rule, value, callback) {
    let reg = /^[0-9\*]*$/;
    if (!value || (value.length == 11 && reg.test(value))) { //为空时不校验
        callback()
    } else {
        callback(new Error("只允许录入11位数字"))
    }
}
//非负数  支持两位小数点
export function validNonnegative(rule, value, callback) {
    let reg = /(^([0-9]+)?(\.[0-9]{1,2})?$)/
    if (reg.test(value) && value < 9999.99) {
        callback()
    } else {
        callback(new Error("只允许录入0-9999.99之间的数据"))
    }
}

/**
 * 验证密码：8-18位，大写字母、小写字母、数字、特殊字符中的2种
 * ^(?![A-Z]+$)(?![a-z]+$)(?!\d+$)(?![\W_]+$)\S+$
 */
export function validPassword(rule, value, callback) {
    let regu = /[ ]+/;
    let reg = /^(?![A-Z]+$)(?![a-z]+$)(?!\d+$)(?![\W_]+$)\S{8,18}$/;
    if (value && regu.test(value)) {
        callback(new Error("新密码不允许录入空格"));
    } else if (value && reg.test(value)) {
        callback()
    } else {
        callback(new Error("请输入8-18位，大写字母、小写字母、数字、特殊字符中的2种"));
    }
}

export function validUrl(rule, value, callback) {
    let reg = /^((https|http):\/\/)(([A-Za-z0-9-~]+)\.)+([A-Za-z0-9-~\/])+$/
    if (reg.test(value)) {
        callback()
    } else {
        callback(new Error("地址格式不正确，请填写以http：//或https://开头的有效连接地址"))
    }
}