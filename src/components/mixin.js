//mixin
export default {
    data() {
        return {

        }
    },
    methods: {
        mixinFun() {
            console.log('mixinFun', '这里是混入的方式 ');
        },
        idGenerator() {
            let qutient = (new Date() - new Date('2020-08-01'))
            qutient += Math.ceil(Math.random() * 1000) // 防止重複
            const chars = '0123456789ABCDEFGHIGKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz';
            const charArr = chars.split("")
            const radix = chars.length;
            const res = []
            do {
                let mod = qutient % radix;
                qutient = (qutient - mod) / radix;
                res.push(charArr[mod])
            } while (qutient);
            return res.join('')
        },   
        //重置nodeID和父级id
        reSetFlowNodeId(json) {
            let _this = this
            function shiftNodeId(item, pid) {
                let id = _this.idGenerator()
                item.nodeId = id
                item.prevId = pid
                if (item.childNode) {
                    shiftNodeId(item.childNode, item.nodeId)
                }
                if (item.conditionNodes) {
                    item.conditionNodes.forEach((element) => {
                        shiftNodeId(element, item.nodeId)
                    })
                }
                return item
            }
            return shiftNodeId(json, 0)
        }
    }
}
