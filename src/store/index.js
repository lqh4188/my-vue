import Vue from 'vue'
import Vuex from 'vuex'
import user from './user'
import test from './test'
import createVuexAlong from 'vuex-along'
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
    },
    mutations: {
    },
    actions: {
    },
    modules: {
        user: user,
        test
    },
    plugins: [createVuexAlong()]

})
