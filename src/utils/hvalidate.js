//自定义指令集
/* 自定义验证 */
export default {}.install = (Vue, options = {}) => {
    //配置msg文字
    let msgConfig = {
        phone: "手机号不正确",
        email: "邮箱不正确",
        idCard: "身份证不正确",
        passWord: "密码由6-12位字母和数字组成",
        max: "长度超过限制",
        min: "字段位数不对",
        required: "不能为空"
    }
    //当有全局定义时采用全局定义
    if (typeof AppConfig != "undefined") {
        msgConfig = AppConfig.msgConfig ? Object.assign(msgConfig, AppConfig.msgConfig) : msgConfig;
    }
    //
    let validateArr = [];
    let validateRule = {};//需要验证的规则 对象 ：{key:phone,Rule:['required','phone']}
    let hval;//用户输入的值 当时
    let hkey;//需要验证的可以 及时
    let validateResult = {};//验证结果
    Vue.directive('hvali', {
        // 只调用一次，指令第一次绑定到元素时调用。在这里可以进行一次性的初始化设置。 
        bind(el, binding, vnode, oldVnode) {
            validateResult = {};
            //console.log("hvalidate>>>>bind", el, binding, vnode, oldVnode);
            validateRule = {};
        },
        // 当被绑定的元素插入到 DOM 中时……
        inserted(el, binding, vnode, oldVnode) {
            validateRule[binding.value.key] = binding.value.rule.split("|");
            validateArr.push(binding.value);
            //获取 需要验证的key和对应的规则
            // console.log("validateRule>>>>", validateRule);
            // console.log("validateArr>>>>", validateArr);
            //keydown键盘按下时 及时检查
            el.addEventListener('keydown', e => {
                setTimeout(() => {
                   // console.log("keydown>>>>", el, e, vnode, oldVnode);
                   console.log("directive>>>>componentUpdated", el, binding, vnode, oldVnode);
                    hval = vnode.data.model.value;
                    hkey = binding.value.key;
                    fhvalidate();
                }, 500)
            }, true);
        },
        // 所在组件的 VNode 更新时调用，但是可能发生在其子 VNode 更新之前。指令的值可能发生了改变，也可能没有。但是你可以通过比较更新前后的值来忽略不必要的模板更新 (详细的钩子函数参数见下)。
        update(el, binding, vnode, oldVnode) {
            //console.log("directive>>>>update");
        },
        //指令所在组件的 VNode 及其子 VNode 全部更新后调用。
        componentUpdated(el, binding, vnode, oldVnode) {
            try {
                if (vnode.data.model.value != oldVnode.data.model.value) {
                    var v = vnode.data.model.value;
                    var keyName = vnode.data.model.expression;
                    var RuleArr = validateRule[keyName];
                    fhvalidate(v, RuleArr, keyName);
                }
            } catch (error) {

            }
        },
        //只调用一次，指令与元素解绑时调用。
        unbind(el, binding, vnode, oldVnode) {
            //console.log("directive>>>>unbind");
            validateRule = {};
            validateResult = {};//验证结果
            validateArr = [];
        }
    })

    /**
 * 
 * @param {*} v 
 * required 必须
 * email 邮箱
 * phone 手机
 * max 最大长度
 * idCard 身份证
 * trim 去空格
 * password 密码
 */
    //统一验证 及时
    function fhvalidate(v, RuleArr, keyName) {
        var msg = "";
        for (var i = 0; i < RuleArr.length; i++) {
            var h = RuleArr[i];
            v += "";
            //手机号
            if (h.indexOf("phone") > -1) {
                var patrn = /^[1][3,4,5,7,8][0-9]{9}$/;
                if (!patrn.test(v) && v.length >= 11) {
                    msg += msgConfig.phone + " ";
                }
            }
            //身份证
            if (h.indexOf("idCard") > -1) {
                var patrn = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
                if (!patrn.test(v) && v.length >= 18) {
                    msg += msgConfig.idCard + " ";
                }
            }
            //最大位数
            if (h.indexOf("max") > -1) {
                if (v) {
                    if ((v + "").length > h.split(":")[1]) {
                        msg += msgConfig.max + " ";
                    }
                }
            }
        }
        if (msg) {
            nshowMsg(msg);
        }
    }
    //显示错误信息
    function nshowMsg(v) {
        try {
            if (v) {
                hToast(v);
            }
        } catch (error) {

        }
        console.log("错误信息", v);
    }
    //
    function getObjbyKey(keyName) {
        var obj = {};
        validateArr.forEach((v) => {
            if (v.key == keyName) {
                obj = v;
            }
        })
        return obj;
    }
    //方式二的验证
    function valiRule(v, RuleArr, keyName) {
        var inputObj = getObjbyKey(keyName);//自定义提示使用
        var msg = "";
        for (var i = 0; i < RuleArr.length; i++) {
            var h = RuleArr[i];
            //必须
            if (h.indexOf("required") > -1) {
                if (!v) {
                    msg += msgConfig.required + " ";
                }
            }
            //手机号
            if (h.indexOf("phone") > -1) {
                var patrn = /^[1][3,4,5,7,8][0-9]{9}$/;
                if (v) {
                    if (!patrn.test(v)) {
                        msg += msgConfig.phone + " ";
                    }
                }

            }
            //身份证
            if (h.indexOf("idCard") > -1) {
                var patrn = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
                if (v) {
                    if (!patrn.test(v)) {
                        msg += msgConfig.idCard + " ";
                    }
                }
            }
            //邮箱
            if (h.indexOf("email") > -1) {
                var patrn = /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/;
                if (v) {
                    if (!patrn.test(v)) {
                        msg += msgConfig.email + " ";
                    }
                }
            }
            //密码6-12位字母和数字组成
            if (h.indexOf("passWord") > -1) {
                var patrn = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/;
                if (!patrn.test(v)) {
                    msg += msgConfig.passWord + " ";
                }
            }
            //最大位数
            if (h.indexOf("max") > -1) {
                if (v) {
                    if ((v + "").length > h.split(":")[1]) {
                        msg += msgConfig.max + " ";
                    }
                }
            }
            //最小位数
            if (h.indexOf("min") > -1) {
                if (v) {
                    if ((v + "").length < h.split(":")[1]) {
                        msg += msgConfig.min + " ";
                    }
                }
            }
            //去掉空格
            if (h.indexOf("trim") > -1) {

            }
        }
        return msg ? inputObj["msg"] ? inputObj["msg"] : msg : msg;//判断是否使用自定义提示
    }
    //方式二的验证
    var beforeSubmitValidate = function (self) {
        var obj = self._data;
        var strMsg = "";
        for (const key in obj) {
            if (validateRule.hasOwnProperty(key)) {
                var val = obj[key];
                var RuleArr = validateRule[key];
                //当val有值得时候
                if (val) {
                    strMsg += valiRule(val, RuleArr, key);
                } else {
                    //当RuleArr含有required的时候
                    if (RuleArr.indexOf("required") > -1) {
                        strMsg += valiRule(val, RuleArr, key);
                    } else {

                    }
                }
            }
        }
        nshowMsg(strMsg);
        return strMsg ? false : true;
    }
    window.beforeSubmitValidate = beforeSubmitValidate;
}

