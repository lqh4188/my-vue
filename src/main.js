import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import axios from './api/index';
import ElementUI from 'element-ui';
import '../src/static/theme/index.css';
import '../src/utils/debounce.js';

//表单验证插件
import Validation from './utils/customValidate.js';
// 全局引入公共方法
import * as utils from './utils/customhelp.js';

import 'xe-utils';
import VXETable from 'vxe-table';
import 'vxe-table/lib/style.css';

import webuitest from 'webui-test';
import 'webui-test/webui.css';
Vue.use(webuitest);

Vue.prototype.$utils = utils;
Vue.use(VXETable);
Vue.use(Validation);
Vue.use(ElementUI);
// Vue.use(CtjInputNumber);
import Slider from 'mini-sliders';
Vue.use(Slider);

import CtjUI from 'ctj-ui';
import 'ctj-ui/ctj-ui.css';
Vue.use(CtjUI);

Vue.prototype.axios = axios;

Vue.config.productionTip = false;
new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app');
