export default {
  functional: true,
  render(h, context) {
    let t = 'h' + context.props.type;
    return <t>{context.slots().default}</t>;
  }
};