module.exports = {
    presets: [
        '@vue/cli-plugin-babel/preset'
    ],
    plugins: [
        "@babel/plugin-proposal-optional-chaining",//解析 可选链语法
        "@babel/plugin-proposal-nullish-coalescing-operator"//解析 双问号语法       
    ]
}
