function nameJoin(name) {
  return `you name id ${name}`;
}
function test() {
  return 'test from comm.js';
}

function test2() {
  return 'test2';
}
function test3() {
  return 'test3';
}

const PI = 3.14;
// 默认导出（每个模块只能有一个）
export default {
  nameJoin,
  test,
  PI
};
// 直接导出
export function funName() {
  return 'you default name is FunName';
}
// 导出列表
export { test2, test3 };
