var configDictionary = {
    'dev': () => require('./config.dev'),
    'development': () => require('./config.dev'),
    'test': () => require('./config.test'),
    'production': () => require('./config.production')
}

var config = configDictionary[process.env.VUE_APP_Server_ENV || 'dev']();
module.exports = config;
