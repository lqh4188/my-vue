//将list转换成tree形式
function getDataByList (data, config) {
    let def = {
        id: 'id', pid: 'parentCode', children: 'children'
    }
    config = Object.assign(def, config);
    var id = config.id || 'id';
    var pid = config.pid || 'parentCode';
    var children = config.children || 'children';
    var idMap = {};
    var jsonTree = [];
    data.forEach(function (v) {
        idMap[v[id]] = v;
    });
    data.forEach(function (v) {
        var parent = idMap[v[pid]];
        if (parent) {
            !parent[children] && (parent[children] = []);
            parent[children].push(v);
        } else {
            jsonTree.push(v);
        }
    });
    return jsonTree;
}

//将后台tree数据，增加label和value
function getDataByTree (data) {
    return data.map(item => {
        item.label = item.name;
        item.value = item.code;
        if (item.children) {
            item.children = getDataByTree(item.children)
        }
        return item
    })
}

export { getDataByList, getDataByTree }