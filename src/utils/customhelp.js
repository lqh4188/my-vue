/*
* 金额格式化：
* number：要格式化的数字
* decimals：保留几位小数
* tdecPoint：小数点符号
* thousandsSep：千分位符号
* */
export function numberFormat(number, decimals = 2, tdecPoint = '.', thousandsSep = ',') {

  number = (number + '').replace(/[^0-9+-Ee.]/g, '');
  if (number.indexOf(',') > -1) {
    return number
  }

  let n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep,
    dec = (typeof tdecPoint === 'undefined') ? '.' : tdecPoint,
    s = '',
    toFixedFix = function (n, prec) {
      let k = Math.pow(10, prec);
      return '' + Math.ceil(n * k) / k;
    };

  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  let re = /(-?\d+)(\d{3})/;
  while (re.test(s[0])) {
    s[0] = s[0].replace(re, "$1" + sep + "$2");
  }

  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}