var base = require('./config.base');
base.url = {
    // portal: 'http://pc-cloudportal-dev-jiaopei.testyf.ak/',
    portal:'http://localhost:8060/'
}
base.cookies.domain = '.testyf.ak';
base.cookies.sessionName = 'cloud_test_sid';
module.exports = base;