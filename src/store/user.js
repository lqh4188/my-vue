
// import { setToken, setUserInfo, getUserInfo, clearStore } from '../utils/token.js';
// import api from '../api/index.js'

const user = {
    state: {
        token: null,
        userInfo: {},
        count: 0,
        views: 10,
    },
    mutations: {
        addViews(state) {
            state.views++
        },
        setState(state, payload) {
            state[payload.value] = payload.data
        },
        setUser: (state, obj) => {
            state.userInfo = obj;
        },
        add: (state) => {
            state.count += 1;
        }
    },
    actions: {
        async setLocalUser({ commit }, flag) {
            let obj = {
                name: '张三',
                age: 30,
                address: '北京海淀'
            }

            commit('setUser', obj.currentUser);

        },


    },
    modules: {
    }
}

export default user;